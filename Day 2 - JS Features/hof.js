// Higher order functions:

// .filter()

const myNumbers = [45, 24, 7, 0, 65, 55, 90, -2, -13]

const positiveNumbers = myNumbers.filter((currentNumber) => {
    return currentNumber >= 0
})

const evenNumbers = myNumbers.filter(num => num % 2 === 0)

console.log("Positives: ", positiveNumbers)
console.log("Evens: ", evenNumbers)


// indexOf

const theIndex = myNumbers.indexOf(0)
console.log(theIndex)