const API_URL = "https://thoughtful-vagabond-fibre.glitch.me/coffee"
let coffees = []
let coffeesAsync = []


function getSomeData(url) {
    fetch(url)
        .then(response => response.json())
        .then(json => {
            coffees = json
            // console.log("Log from inside promise context", coffees)
        })
        .catch(error => console.error("THIS IS OUR ERROR!", error.message))
}

getSomeData(API_URL)
// console.log("Log from global context", coffees)

// ASYNC FUNCTION
async function getSomDataAsync(url) {
    try {
        const response = await fetch(url)
        const json = await response.json()
        console.log("Log from inside the async function", json)
        
        return json
    }
    catch(error) {
        console.error(error.message)
    }
}

coffeesAsync = getSomDataAsync(API_URL)
console.log("Log from global context", coffeesAsync)