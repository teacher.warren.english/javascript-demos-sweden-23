// DOM Manipulation
const buttonChangeElement = document.getElementById("btn-change")
const paragraphElement = document.getElementById("para-text")
const listElement = document.getElementById("coffee-list")
const inputNameElement = document.getElementById("input-name")

// Add an event listener
buttonChangeElement.addEventListener('click', handleButtonClick)
// inputNameElement.addEventListener('change', (event) => updateParagraphText(event))

function handleButtonClick() {
    console.log("It worked!")
    
    updateParagraphText()

    // change list contents
    const coffees = ["Latte", "Cappuccino", "Mocha"]
    
    // update the list element using renderCoffees()
    renderCoffees(coffees)
}

function renderCoffees(coffeeList) {
    // reset the parent <ul>
    listElement.innerHTML = ''
    
    for (const currentCoffee of coffeeList) {
        // create new <li> element
        const newListElement = document.createElement('li')
        // set its inner text to the current coffee
        newListElement.innerText = currentCoffee
        
        // add it to the parent <ul> element
        listElement.appendChild(newListElement)
    }
}

function updateParagraphText() {
    // changing p text
    paragraphElement.innerText = inputNameElement.value
    // paragraphElement.innerText = "This text has been changed by the button click!"
}