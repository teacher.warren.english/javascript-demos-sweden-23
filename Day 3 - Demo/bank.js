// Revealing module pattern

let bankBalance = 0

function getBalance() {
    return bankBalance
}

function deposit(amount) {
    bankBalance += amount
}

function withdraw(amount) {
    bankBalance -= amount
}

const bank = {
    getBalance,
    deposit,
    withdraw
}

export default bank