import bank from './bank.js'
import { API_URL } from './utils.js'

// DOM Elements
const bankBalanceElement = document.getElementById("bank-balance")
const btnDepositElement = document.getElementById("btn-deposit")
const btnWithdrawElement = document.getElementById("btn-withdraw")
const coffeeListElement = document.getElementById("select-coffees")
const coffeeDescriptionElement = document.getElementById("coffee-description")
const coffeePriceElement = document.getElementById("coffee-price")

// Event Listeners
// btnDepositElement.addEventListener('click', handleDepositClick)
// btnWithdrawElement.addEventListener('click', handleWithdrawClick)
btnDepositElement.addEventListener('click', () => handleDepositClick(100))
btnWithdrawElement.addEventListener('click', () => handleWithdrawClick(50))
coffeeListElement.addEventListener('change', handleCoffeeSelected)

// Variables
let coffees = []

// Event Handlers
function handleDepositClick(amount) {
    // do stuff
    bank.deposit(amount)
    displayBankBalance()
}

function handleWithdrawClick(amount) {
    // handle a withdraw
    if (bank.getBalance() >= amount) {
        bank.withdraw(amount)
        displayBankBalance()
    }
    else
        alert("Sorry, you don't have the available funds 💀")
}

function handleCoffeeSelected(event) {
    // console.log(event.target.value);
    const currentSelectedCoffee = coffees.find(coffee => coffee.id == event.target.value)

    coffeeDescriptionElement.innerText = currentSelectedCoffee.description
    coffeePriceElement.innerText = `$${currentSelectedCoffee.price}`
    // Alt + left click
}

// Functions
function displayBankBalance() {
    // get balance from bank object
    bankBalanceElement.innerText = `Your balance is $${bank.getBalance()}`
}

function getCoffees() {
    fetch(API_URL)
        .then(response => response.json())
        .then(json => {
            // console.log(json)
            coffees = json
            populateCoffeeSelectBox()
        })
        .catch(error => console.error(error.message))
}


function populateCoffeeSelectBox() {
    // Basic validation
    if (!coffees)
        alert("No coffees to display ❌")
    
    for (const coffee of coffees) {
        let newCoffeeOption = document.createElement('option')
        newCoffeeOption.innerText = coffee.description
        // wire up a value
        newCoffeeOption.value = coffee.id

        coffeeListElement.appendChild(newCoffeeOption)
    }
}






// Runtime
getCoffees()